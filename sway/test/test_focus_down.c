#include <stdio.h>
#include <string.h>
#include "framework.c"

int main() {
	initialize();

	// H[V[one T[*two]] T[three]]
	// should switch to 'first' vertically,
	// which is the top-left kitty here.
	struct sway_workspace *ws = workspace_create(root->outputs->items[0], "WS1");
	ws->layout = L_HORIZ;
	struct sway_container *one = container_create(NULL);
	struct sway_container *two = container_create(NULL);

	struct sway_container *onetwo = container_create(NULL);
	onetwo->current.layout = L_VERT;
	onetwo->pending.layout = L_VERT;

	container_add_child(onetwo, one);
	container_add_child(onetwo, two);
	workspace_add_tiling(ws, onetwo);
	fprintf(stderr, "first child of onetwo is %p\n", onetwo->pending.children->items[0]);

	struct sway_container *three = container_create(NULL);
	workspace_add_tiling(ws, three);
	config->handler_context.node = &two->node;

	seat_set_focus(&default_seat, &two->node);
	list_t *results = execute_command("focus down", NULL, two);
	
	int n_results = results->length;
	if (n_results == 0) {
		printf("did not receive any result\n");
		return -1;
	}
	for (int i=0; i<n_results; i++) {
		struct cmd_results *result = results->items[i];
		if (result->status != CMD_SUCCESS) {
			printf("unsuccessful result: '%s'\n", result->error);
			return -1;
		}
	}
	if (seat_get_focus(&default_seat) == &one->node)
		return 0;
	else {
		printf("not ok other node was focused\n");
		return -1;
	}
}
