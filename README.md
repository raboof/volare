# volare

Volare is a tabbed, tiling [Wayland] compositor.

This means, compared to other tiling compositors, volare is more static: new windows will show up as tabs in the current frame instead of rearranging the screen layout.

## Project status

This project is in its bootstrap phase: it should be basically functional, but is still poorly documented and might have surprising behaviour in a number of places. Feel free to try it out, report any [Issues](https://codeberg.org/raboof/volare/issues), or jump in and fix some :).

Contributors are welcome!

## Reporting issues

Volare has an 'all issues welcome' policy: reports that are likely (or even confirmed) to be 'upstream' issues are still welcome to be tracked [here](https://codeberg.org/raboof/volare/issues).
This makes the volare issue tracker a place where the community can share diagnostics, workarounds and alternatives.

Wishlist items are also welcome.

This means over time the issue list will grow to contain many items that are unlikely to be addressed. We do not consider this a problem: they are an invitation to move the work forward if you happen to care about that particular issue. We will only close issues that are no longer reproducible, or where we have weighed the pros and cons and decided we actually prefer the current behavior.

### Reporting security issues

If you have found a potential security issue, please report it
privately by email to volare [at] bzzt.net .

## Provenance

Volare is a branch of [sway], with relatively minor changes to make it behave more like [Notion]. This means we take advantage of the great sway codebase and the work done to make other Wayland tools integrate well with it. All the bugs are ours, all the complicated bits theirs ;).

## FAQ

Q: Do you plan to make your changes available upstream?

A: Definitely! In a perfect world volare would just be another configuration for sway. In practice, however, for many of the changes supporting both behaviors side-by-side from the same codebase would make things needlessly complex, so we expect those not make sense for sway, and to maintain these as a fork instead. That said, we do prefer to keep the delta as small as possible and share patches upstream where possible.

Q: Do you plan to keep tracking upstream development?

A: Yes, we plan to merge upstream into our branch at irregular intervals.

Q: Will you rename all sway tooling commands?

A: The tools, manpages, and various other files that ship with sway have been renamed so that an installation of volare won't conflict with an installation of sway. We don't expect to make changes to the APIs between various tools and the compositor, however. Volare also continues to use `SWAYSOCK` as the name of the environmental variable containing the path to its IPC socket. This means that any existing tools you may have that are designed to work with the sway IPC socket should continue to work with volare.

### Installation on NixOS

The NixOS integration relies on a generalization of the sway
module, https://nixpk.gs/pr-tracker.html?pr=259158

#### When using flakes for your NixOS configuration

Pass volare to your configuration from `/etc/nixos/flake.nix`:

```
{
  inputs.volare = {
    url = "git+https://codeberg.org/raboof/volare";
    inputs.nixpkgs.follows = "nixpkgs";
  };
  outputs = { self, nixpkgs, volare }@attrs: {
    nixosConfigurations.rigter = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = attrs;
      modules = [ ./configuration.nix ];
    };
  };
}
```

And then use the `sway` module:

```
{ config, pkgs, lib, volare, ... }:

(...)

  programs.sway = {
    enable = true;
    package = (pkgs.sway.override {
      sway-unwrapped = volare.packages.x86_64-linux.default;
    };
    extraPackages = with pkgs; [ sway swaylock swayidle swaybg foot dmenu i3status ];
  };
```

#### When using a classic NixOS configuration

The downside of using a classic NixOS configuration is that it will
take the `nixpkgs` specified in the `flake.lock`, which might be
different from the one you're using. See https://github.com/NixOS/nix/issues/9154
for more information about this.

Use the following, replacing a84e9c68aba236dc90a03a2117dcb5a79cb955a2 with
the latest `volare` revision:

```
  programs.sway = {
    enable = true;
    package = (pkgs.sway.override {
      sway-unwrapped = builtins.getFlake "git+https://codeberg.org/raboof/volare?rev=a84e9c68aba236dc90a03a2117dcb5a79cb955a2").packages.x86_64-linux.default
    };
    extraPackages = with pkgs; [ sway swaylock swayidle swaybg foot dmenu i3status ];
  }
```

### Compiling from Source

For now, check out the sway wiki's [Development Setup] instructions if you want
to build the HEAD of volare and wlroots for testing or development.

Install dependencies:

* meson \*
* [wlroots]
* wayland
* wayland-protocols \*
* pcre2
* json-c
* pango
* cairo
* gdk-pixbuf2 (optional: additional image formats for system tray)
* [swaybg] (optional: wallpaper)
* [scdoc] (optional: man pages) \*
* git (optional: version info) \*

_\* Compile-time dep_

Run these commands:

    meson build/
    ninja -C build/
    sudo ninja -C build/ install

## Configuration

If you already use i3 or sway, then copy your config to
`~/.config/volare/config` and it should work out of the box. Otherwise, copy
the sample configuration file to `~/.config/volare/config`. It is usually
located at `/etc/volare/config`. Run `man 5 volare` for information on the
configuration.

If your configuration uses the `swaybar`, `swaymsg`, or `swaynag` commands, it
can be updated to use `volarebar`, `volaremsg`, and `volarenag` instead.

## Running

Run `volare` from a TTY. Some display managers may work but are not supported
(gdm is known to work fairly well).

[sway]: https://swaywm.org
[Notion]: https://notionwm.net
[Wayland]: https://wayland.freedesktop.org/
[Development Setup]: https://github.com/swaywm/sway/wiki/Development-Setup
[wlroots]: https://gitlab.freedesktop.org/wlroots/wlroots
[swaybg]: https://github.com/swaywm/swaybg/
[scdoc]: https://git.sr.ht/~sircmpwn/scdoc
