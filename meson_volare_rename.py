#!/usr/bin/env python3

import json
import os
import re
import subprocess
import sys

regex = re.compile('(/man/man[1-9]/.*\.[1-9]$|/completions/)')

sed_patterns = (
    "-e 's, (http://swaywm.org),,g'",
    "-e 's/sway/volare/g'",
    "-e 's/Sway/Volare/g'",
)
cmd_sed = "sed -i " + ' '.join(sed_patterns)

destdir = os.getenv('DESTDIR', '')

meson_build_root = os.getenv('MESON_BUILD_ROOT')
if not meson_build_root:
    sys.stderr.write('Environment variable MESON_BUILD_ROOT must be set.\n')
    os.exit(1)

mesonintrospect = os.getenv('MESONINTROSPECT')
if not mesonintrospect:
    sys.stderr.write('Environment variable MESONINTROSPECT must be set.\n')
    os.exit(1)

cmd_get_installed_files = f'{mesonintrospect} {meson_build_root} --installed'
process = subprocess.run(cmd_get_installed_files, shell=True, capture_output=True, check=True)
installed_files = json.loads(process.stdout)

for fname in installed_files.values():
    if regex.search(fname):
        subprocess.run(f'{cmd_sed} {destdir}{fname}', shell=True, check=True)
